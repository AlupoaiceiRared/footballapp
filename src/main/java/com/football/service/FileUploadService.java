package com.football.service;

import com.football.domain.FileUpload;
import com.football.domain.League;
import com.football.domain.Player;
import com.football.domain.Team;
import com.football.repository.FileUploadRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Consumer;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class FileUploadService {

    @Autowired
    private FileUploadRepository fileUploadRepository;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private PlayerService playerService;

    public FileUpload saveUploadedFileInformation(MultipartFile file, String path, String date, Integer year, Integer month) {
        FileUpload fileToUpload = new FileUpload();
        fileToUpload.setFilename(file.getOriginalFilename());
        fileToUpload.setPath(path);
        fileToUpload.setYearOfTheFile(year);
        fileToUpload.setMonthOfTheFile(month);
        fileToUpload.setUploadDate(date);
        log.info("File " + fileToUpload.getFilename() + " month/year " + fileToUpload.getMonthOfTheFile() + "/" + fileToUpload.getYearOfTheFile()
                + " with path " + fileToUpload.getPath());
        return fileUploadRepository.save(fileToUpload);
    }

    public FileUpload updateFileWithInformation(MultipartFile file, String path, String date, Integer year, Integer month, String filename) {
        List<FileUpload> fileUploadList = fileUploadRepository.findByFilenameLikeAndYearOfTheFileLike(filename, year);
        if (fileUploadList.size() > 1) {
            log.error("Filename exists !");
            return null;
        } else {
            FileUpload fileUpload = fileUploadList.get(0);
            fileUpload.setUploadDate(date);
            fileUpload.setYearOfTheFile(year);
            fileUpload.setMonthOfTheFile(month);
            log.info("File " + filename + " month/year " + month + "/" + year
                    + " with path " + path);
            return fileUploadRepository.save(fileUpload);

        }
    }

    public List<FileUpload> findAll() {
        return fileUploadRepository.findAll();
    }

    public List<FileUpload> findAllByYear(Integer year) {
        return fileUploadRepository.findByYearOfTheFile(year);
    }

    public FileUpload findById(String id) {
        return fileUploadRepository.findById(id).orElse(null);
    }


    public boolean verifyOnlyOneFileExistsWithNameAndYear(String name, Integer year) {
        List<FileUpload> files = fileUploadRepository.findByFilenameLikeAndYearOfTheFileLike(name, year);

        return files.size() == 1 ? true : false;
    }

    public FileUpload findFileByMonthAndYear(Integer year, Integer month) {
        List<FileUpload> filesForMonthAndYear = fileUploadRepository.findByMonthOfTheFileLikeAndYearOfTheFileLike(month, year);
        if (filesForMonthAndYear.size() == 1) {
            return filesForMonthAndYear.get(0);
        } else {
            return null;
        }
    }

    public FileUpload findByFilename(String filename) {
        return fileUploadRepository.findByFilename(filename);
    }

    public void deleteFile(FileUpload fileToDelete) {
        fileUploadRepository.delete(fileToDelete);
    }

    public void readFileAndMapValues(String filename) {

        FileUpload fileUpload = findByFilename(filename);
        Pattern pattern = Pattern.compile(",");

        try {
            Stream<String> stream = Files.lines(Paths.get(fileUpload.getPath()));
            Stream<String> stream2 = Files.lines(Paths.get(fileUpload.getPath()));

            //stream.forEach(System.out::println);
            String firstLine = stream.findFirst().get();
            String secondLine = stream2.limit(2).skip(1).collect(Collectors.joining());
            List<League> leagueList = leagueService.findByName(secondLine);
            System.out.println(leagueList.size());
            List<Team> teamList = teamService.findByName(secondLine);
            if(leagueList.size()!=0){
                League league = leagueService.findByNameOneResult(secondLine);
                saveDataForTeam(fileUpload,pattern,league);
            } else if(teamList.size() != 0){
                Team team = teamService.findByNameOneResult(secondLine);
                saveDataForPlayer(fileUpload,pattern,team);
                System.out.println("dada");
            }


        } catch (IOException e) {
            System.out.println(String.valueOf(e));
        }
    }

    public void saveDataForTeam(FileUpload fileUpload, Pattern pattern, League league) throws IOException {
            Stream<String> stream = Files.lines(Paths.get(fileUpload.getPath()));
            List<Team> teams = stream.skip(2).map(line -> {
                String[] arr = pattern.split(line);
                return new Team(
                        String.valueOf(arr[0]),
                        0);
            }).collect(Collectors.toList());

            Consumer<Team> save = team ->
            {
                teamService.CreateTeam(team);
            };

            Consumer<Team> saveTeam = team -> teamService.CreateTeam(team);
            Consumer<Team> addTeamToLeague = team -> teamService.UpdateTeamLeague(team, league);

            teams.forEach(saveTeam);
            teams.forEach(addTeamToLeague);
    }

    public void saveDataForPlayer(FileUpload fileUpload, Pattern pattern, Team team) throws IOException {
        Stream<String> stream = Files.lines(Paths.get(fileUpload.getPath()));
        List<Player> players = stream.skip(2).map(line -> {
            String[] arr = pattern.split(line);
            return new Player(
                    String.valueOf(arr[0]),
                    String.valueOf(arr[1]),
                    Integer.valueOf(arr[2]),
                    String.valueOf(arr[3]),
                    Float.valueOf(arr[4]),
                    Float.valueOf(arr[5]),
                    Float.valueOf(arr[6]),
                    Float.valueOf(arr[7]),
                    Float.valueOf(arr[8]),
                    Boolean.valueOf(arr[9]),
                    Boolean.valueOf(arr[10]));
        }).collect(Collectors.toList());


        Consumer<Player> savePlayer = player -> playerService.createPlayer(player);
        Consumer<Player> addPlayerToTeam = player -> playerService.updatePlayerTeam(player,team);

        players.forEach(savePlayer);
        players.forEach(addPlayerToTeam);
    }
}
