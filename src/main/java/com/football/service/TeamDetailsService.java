package com.football.service;

import com.football.domain.TeamDetails;
import com.football.repository.TeamDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TeamDetailsService {

    @Autowired
    private TeamDetailsRepository teamDetailsRepository;

    public void save(TeamDetails teamDetails){
        teamDetailsRepository.save(teamDetails);
    }

    public TeamDetails findByYearRoundAndTeamId(int roundNumber, int year, int teamId){
        return teamDetailsRepository.findByRoundAndYearAndTeam(roundNumber,year,teamId);
    }
}
