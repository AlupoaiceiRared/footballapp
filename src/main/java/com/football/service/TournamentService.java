package com.football.service;


import com.football.domain.League;
import com.football.domain.Match;
import com.football.domain.Round;
import com.football.domain.Tournament;
import com.football.repository.TournamentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Slf4j
public class TournamentService {

    @Autowired
    private TournamentRepository tournamentRepository;

    @Autowired
    private RoundService roundService;

    @Autowired
    private LeagueService leagueService;

    public Tournament save(Tournament tournament){
        return tournamentRepository.save(tournament);
    }

    public Tournament findById(int id){
        return tournamentRepository.findById(id).orElse(null);
    }


    public List<Round> initializeRound(){

        List<Round> roundList = new ArrayList<>();
        List<League> currentLeagues = leagueService.findAll();
        for(League league : currentLeagues){
            if(league.getTournamentList().size() != 0){
                List<Tournament> tournamentList = league.getTournamentList();
                for(Tournament tournament : tournamentList){
                    if(tournament.getOngoing()){
                        Round round = new Round(tournament.getLastRoundPlayed()+1,tournament.getYear(),tournament);
                        List<Match> matches = new ArrayList<>();
                        round.setMatches(matches);
                        roundService.saveRound(round);
                        tournament.getRoundList().add(round);
                        tournament.setLastRoundPlayed(tournament.getLastRoundPlayed()+1);
                        save(tournament);
                        roundList.add(round);
                    }
                }
            }
        }
        return roundList;
    }

    public List<Tournament> verifySeasonEnd(){
        List<Tournament> tournamentsEnded = new ArrayList<>();
        List<League> currentLeagues = leagueService.findAll();
        for(League league : currentLeagues){
            if(league.getTournamentList().size() != 0){
                List<Tournament> tournamentList = league.getTournamentList();
                for(Tournament tournament : tournamentList){
                    if(tournament.getOngoing()){
                        if(tournament.getLastRoundPlayed() == tournament.getNrOfTeams() * 2 - 2){
                            tournament.setOngoing(false);
                            tournamentRepository.save(tournament);
                            tournamentsEnded.add(tournament);
                        }
                    }
                }
            }
        }
        return tournamentsEnded;
    }

    public Boolean verifySeasonEndAcrossGlobe(){
        List<League> currentLeagues = leagueService.findAll();
        for(League league : currentLeagues){
            if(league.getTournamentList().size() != 0){
                List<Tournament> tournamentList = league.getTournamentList();
                boolean allTournamentsEnded = true;
                for(Tournament tournament : tournamentList){
                    if(tournament.getOngoing()) {
                        allTournamentsEnded = false;
                        return false;
                    }
                }
                log.info("Season for  " + league.getName() + " has ended!" );
                league.setOngoingSeason(false);
                leagueService.Update(league);
            }
        }
        return true;
    }


    public List<Tournament> findLastTournaments(){
        List<League> leagueList = leagueService.findAll();
        List<Tournament> lastTournaments = new ArrayList<>();
        for(League league : leagueList){
            if(league.getTournamentList().size() != 0){
                List<Tournament> tournamentList = league.getTournamentList();
                tournamentList.sort(Comparator.comparing(Tournament::getYear).reversed());
                lastTournaments.add(tournamentList.get(0));
            }
        }

        return lastTournaments;
    }

    public Tournament findTournamentFromLeagueWithYear(int year, List<Tournament> tournaments){
        Tournament tournamentNull = null;
        for(Tournament tournament : tournaments){
            if(tournament.getYear() == year){
                return tournament;
            }
        }
        return tournamentNull;
    }
}
