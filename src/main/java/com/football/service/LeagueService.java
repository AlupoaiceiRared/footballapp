package com.football.service;

import com.football.domain.League;
import com.football.domain.Team;
import com.football.domain.Tournament;
import com.football.domain.User;
import com.football.repository.LeagueRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Slf4j
public class LeagueService {

    @Autowired
    private LeagueRepository leagueRepository;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private TeamService teamService;

    public void CreateLeague(League league){

        List<Team> teams = new ArrayList<>();
        league.setTeamList(teams);
        league.setOngoingSeason(false);
        leagueRepository.save(league);

    }

    public void Update(League league){
        leagueRepository.save(league);
    }


    public void UpdateTeamOldToNew(String oldName, String name){
        League league = findByNameOneResult(oldName);
        if(league != null){
            league.setName(name);
            leagueRepository.save(league);
        } else {
            log.error("There are no leagues with this name");
        }
    }

    public League findById(int id){
        return leagueRepository.findById(id).orElse(null);
    }

    public League findByNameOneResult(String name){
        return  leagueRepository.findByName(name);
    }

    public void UpdateLeague(League league, Team team){
        List<Team> teams = league.getTeamList();
        System.out.println(teams.size());

        teams.add(team);
        System.out.println(team.getName());

        league.setTeamList(teams);
        System.out.println(teams.size());

        long noOfTeams = teams.size();
        System.out.println("NoofTeams" + noOfTeams);
        long noOfRounds = noOfTeams * 2 - 2 ;
        league.setNoOfTeams(noOfTeams);
        league.setNoOfRounds(noOfRounds);
        leagueRepository.save(league);
    }
    public List<League> findByName(String name) {
        return leagueRepository.findByNameLike(name);
    }

    public void deleteLeague(League league){
        leagueRepository.delete(league);
    }

    public List<League> findAll(){
        return (List<League>) leagueRepository.findAll();
    }


    public void initLeaguePerSeason(int year){
    List<League> leagueList = findAll();
        for(League league : leagueList) {
            if (league.getTournamentList().size() == 0){
                if(!league.getOngoingSeason()) {
                    //for new season reset points for all teams in the current league
                    for(Team team : league.getTeamList()){
                        team.setPoints(0);
                        teamService.UpdateTeam(team);
                    }
                    Tournament tournament = new Tournament((int) league.getNoOfTeams(), (int) league.getNoOfRounds(), year, league);
                    String[][] table = new String[(int) league.getNoOfTeams()][(int) league.getNoOfTeams()];
                    for (int i = 0; i < (int) league.getNoOfTeams(); i++) {
                        for (int j = 0; j < (int) league.getNoOfTeams(); j++) {
                            table[i][i] = "-   ";
                        }
                    }
                    tournament.setTable(table);
                    tournament.setNrOfTeams((int) league.getNoOfTeams());
                    tournamentService.save(tournament);
                    league.setOngoingSeason(true);
                    Update(league);
                }
            } else {
                for(Tournament tournament : league.getTournamentList()){
                    if(tournament.getYear() == year){
                        break;
                    } else {
                        if(!league.getOngoingSeason()) {
                            Tournament tournamentInit = new Tournament((int) league.getNoOfTeams(), (int) league.getNoOfRounds(), year, league);
                            String[][] table = new String[(int) league.getNoOfTeams()][(int) league.getNoOfTeams()];
                            for (int i = 0; i < (int) league.getNoOfTeams(); i++) {
                                for (int j = 0; j < (int) league.getNoOfTeams(); j++) {
                                    table[i][i] = "-   ";
                                }
                            }
                            tournamentInit.setTable(table);
                            tournamentService.save(tournamentInit);
                            league.setOngoingSeason(true);
                            Update(league);
                        }
                    }
                }
            }
        }
    }
}

