package com.football.controller;


import com.football.domain.*;
import com.football.export.ExcelExporter;
import com.football.export.FootballData;
import com.football.export.FootballDataExportService;
import com.football.mail.EmailService;
import com.football.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.GeneratedValue;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.websocket.server.PathParam;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private UserService userService;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private TeamService teamService;

    @Autowired
    private PlayerService playerService;

    @Autowired
    private RoundService roundService;

    @Autowired
    private FootballDataExportService footballDataExportService;

    @Autowired
    private EmailService emailService;

    @GetMapping("/")
    public String showIndexPage(){
        return "index";
    }


    @GetMapping("/login")
    public String login(Model model)
    {
        return "loginForm";
    }

    @GetMapping("/register")
    public String registerForm(Model model){
        model.addAttribute("user", new User());

        return "registerForm";
    }

    @PostMapping("/register")
    public String registerUser(@Valid User user, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            return "registerForm";
        }

        if(userService.isUserPresent(user.getEmail())){
            model.addAttribute("exists", true);
            return "registerForm";
        }
        else{
            userService.createUser(user);
            //return "registerForm";
            return  "succes";
        }
    }

    //cautarea dupa nume
    @GetMapping("/users")
    public String listUsers(Model model, @RequestParam(defaultValue = "") String name){

        model.addAttribute("users", userService.findByName("%" + name + "%"));
        return  "userslist";
    }

    @GetMapping("/leagues")
    public String listLeagues(Model model, @RequestParam(defaultValue = "") String name){
        model.addAttribute("leagues", leagueService.findByName("%" + name + "%"));

        return "leaguelist";
    }

    @GetMapping("/teams")
    public String listTeams(Model model, @RequestParam(defaultValue = "") String name){
        model.addAttribute("teams", teamService.findByName("%" + name + "%"));

        return "teamlist";
    }

    @GetMapping("/players")
    public String listPlayers(Model model, @RequestParam(defaultValue = "") String name){
        model.addAttribute("players", playerService.findByBothNames("%" + name + "%"));

        return "playerlist";
    }

    @GetMapping("/operations")
    public String operationsPanel(Model model)
    {
        return "operationsPanel";
    }

    @GetMapping("/matchday")
    public String matchdayShow(Model model){

        List<Round> roundList = new ArrayList<>();
        //Round test = new Round();
        List<League> activeLeagues = leagueService.findAll();
        for(League league : activeLeagues) {
            if (league.getTournamentList().size() != 0) {
                List<Tournament> tournamentList = league.getTournamentList();
                tournamentList.sort(Comparator.comparing(Tournament::getYear).reversed());
                Tournament tournament = tournamentList.get(0);
                //test = roundService.findByRoundNumberAndYear(5,2021);
                Round round = roundService.findByRoundNumberAndYear(tournament.getLastRoundPlayed(), tournament.getYear(), tournament.getId());
                if(round != null){
                    roundList.add(round);
                }
            }
        }
        System.out.println(roundList.size());
        //roundList.add(test);
        /*System.out.println(roundList.size());
        for(Round r: roundList){
            System.out.println(r.getRoundNumber() + "    " + r.getTournament().getLeague().getName());
        }*/
        model.addAttribute(roundList);
        return "matchday";
    }

    @GetMapping("/leagueTable")
    public String showLeagues(Model model){
        List<League> leagueList = leagueService.findAll();
        model.addAttribute(leagueList);
        return "tableOfLeagues";
    }

    @GetMapping("/about")
    public String about(Model model){
        return "about";
    }

    @GetMapping("/table")
    public String showLeagueTable(@PathParam("name") String name, Model model){

        League league = leagueService.findByNameOneResult(name);
        if(league != null){
            List<Team> teamList = league.getTeamList();
            teamList.sort(Comparator.comparing(Team::getPoints).reversed());
            model.addAttribute(teamList);
            return "LeagueTable";
        }
        return "failedToInitializeNewSeason";
    }

    @GetMapping("/downloadPage")
    public String showDownloadPage(Model model){

        List<Integer> yearList = new ArrayList<>();
        List<League> leagueList = leagueService.findAll();
        model.addAttribute("leagueList",leagueList);
        for(League league : leagueList){
            if(league.getTournamentList().size() != 0){
                List<Tournament> tournamentList = league.getTournamentList();
                for(Tournament tournament : tournamentList){
                    if(!yearList.contains(tournament.getYear())){
                        yearList.add(tournament.getYear());
                    }
                }
            }
        }

        model.addAttribute("yearList" , yearList);
        return "downloadPage";
    }

    @PostMapping("/downloadStats")
    public String downloadStats(@RequestParam("name") String name, @RequestParam("year") int year, HttpServletResponse response) throws IOException {

        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=leagueResults_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);


        List<FootballData> footballDataList = footballDataExportService.getFootballDataWithLeague(name, year);

        ExcelExporter excelExporter = new ExcelExporter(footballDataList);

        excelExporter.export(response);

        return null;
    }

    @PostMapping("/subscribe")
    public String subscribe(@RequestParam("username") String username, @RequestParam("leagueName") String leagueName){
        User user = userService.findById(username);
        if(user != null){
            user.setLeaguePreference(leagueName);
            userService.updateUser(user);
        }
        return "succes";
    }

    @GetMapping("/subscribePage")
    public String viewSubscribePage(Model model){
        List<League> leagueList = leagueService.findAll();
        model.addAttribute("leagueList", leagueList);
        return "subscribePage";
    }

    @GetMapping("/sendMail")
    public void mailSend(){
        emailService.sendSimpleMessage("ra","da", "salut tatic");
    }

}
