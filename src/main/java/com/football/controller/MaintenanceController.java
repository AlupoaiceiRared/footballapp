package com.football.controller;

import com.football.domain.*;
import com.football.export.ExcelExporter;
import com.football.export.FootballData;
import com.football.export.FootballDataExportService;
import com.football.mail.EmailService;
import com.football.repository.TeamDetailsRepository;
import com.football.repository.TeamRepository;
import com.football.service.LeagueService;
import com.football.service.TournamentService;
import com.football.service.UserService;
import com.football.serviceUtils.FootballUtils;
import com.football.serviceUtils.TeamUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;


@Controller
@Slf4j
public class MaintenanceController {

    @Autowired
    private FootballUtils footballUtils;

    @Autowired
    private TeamRepository teamRepository;

    @Autowired
    private TeamUtils tu;

    @Autowired
    private TeamDetailsRepository teamDetailsRepository;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private FootballDataExportService footballDataExportService;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;


    @GetMapping("/generate")
    public void Generate() {

        List<Team> teamList = new ArrayList<>();
        Team t1 = new Team("Manchester United", 0);
        Team t2 = new Team("Chelsea", 0);
        Team t3 = new Team("Wolverhampton", 0);
        Team t4 = new Team("Arsenal", 0);
        Team t5 = new Team("Manchester City", 0);
        Team t6 = new Team("Southampton", 0);
        Team t7 = new Team("Leicester", 0);
        Team t8 = new Team("Leeds United", 0);
        Team t9 = new Team("Liverpool", 0);
        Team t10 = new Team("Everton", 0);
        Team t11 = new Team("West Ham United", 0);
        Team t12 = new Team("Aston Villa", 0);
        Team t13 = new Team("Tottenham Hotspur", 0);
        Team t14 = new Team("Crystal Palace", 0);
        Team t15 = new Team("Burnley", 0);
        Team t16 = new Team("Brighton", 0);
        Team t17 = new Team("Fulham", 0);
        Team t18 = new Team("West Bromwich Albion", 0);
        Team t19 = new Team("Sheffield United", 0);
        Team t20 = new Team("Newcastle United", 0);

        teamList.add(t1);
        teamList.add(t2);
        teamList.add(t3);
        teamList.add(t4);
        teamList.add(t5);
        teamList.add(t6);
        teamList.add(t7);
        teamList.add(t8);
        teamList.add(t9);
        teamList.add(t10);
        teamList.add(t11);
        teamList.add(t12);
        teamList.add(t13);
        teamList.add(t14);
        teamList.add(t15);
        teamList.add(t16);
        teamList.add(t17);
        teamList.add(t18);
        teamList.add(t19);
        teamList.add(t20);

        //footballUtils.MatchesGenerator(teamList);
    }

    @GetMapping("/testFormation")
    public void testFormation() {

        List<Team> teams = teamRepository.findByNameLike("Arsenal");
        HashMap<String, List<Player>> mapF;
        mapF = tu.TeamBuild(Formation.F442, teams.get(0));
        List<Player> tt = mapF.get("GK");
        for (Player pppp : tt) {
            System.out.println(pppp.getFirstname());
        }

        for (Map.Entry<String, List<Player>> entry : mapF.entrySet()) {
            List<Player> list = entry.getValue();
            System.out.println(entry.getKey());
            for (Player p : list) {
                System.out.print(p.getLastname() + "  " + p.getRating() + "  | ");
            }
            System.out.println(" ");
        }
    }


    @GetMapping("/testScore")
    public void testScore() {
        System.out.println("-------------------Arsenal-------------------");
        List<Team> teams = teamRepository.findByNameLike("Arsenal");
        HashMap<String, List<Player>> mapF = new HashMap<>();
        mapF = tu.TeamBuild(Formation.F442, teams.get(0));
        TeamDetails td1 = new TeamDetails();
        td1.setRound(1);
        td1.setTeam(teamRepository.findByNameOneResult("Arsenal"));
        td1.setYear(2021);
        td1.setConfigurationPerMatchMap(mapF);
        teamDetailsRepository.save(td1);

        for (Map.Entry<String, List<Player>> entry : mapF.entrySet()) {
            List<Player> list = entry.getValue();
            System.out.println(list.size());
            System.out.println(entry.getKey());
            for (Player p : list) {
                System.out.print(p.getLastname() + "  " + p.getRating() + "  | ");
            }
            System.out.println(" ");
        }

        System.out.println("-------------------Chelsea-------------------");
        List<Team> teams2 = teamRepository.findByNameLike("Chelsea");
        System.out.println("ch1");
        HashMap<String, List<Player>> mapF2 = new HashMap<>();
        System.out.println("ch2");
        mapF2 = tu.TeamBuild(Formation.F433, teams2.get(0));
        System.out.println("ch3");

        TeamDetails td2 = new TeamDetails();
        System.out.println(teamRepository.findByNameOneResult("Chelsea").getName());
        td2.setRound(1);
        td2.setTeam(teamRepository.findByNameOneResult("Chelsea"));
        td2.setYear(2021);
        td2.setConfigurationPerMatchMap(mapF2);
        teamDetailsRepository.save(td2);

        for (Map.Entry<String, List<Player>> entry2 : mapF2.entrySet()) {
            List<Player> list2 = entry2.getValue();
            System.out.println(entry2.getKey());
            for (Player p : list2) {
                System.out.print(p.getLastname() + "  " + p.getRating() + "  | ");
            }
            System.out.println(" ");
        }

        Team arsenal = teamRepository.findByNameOneResult("Arsenal");
        Team chelsea = teamRepository.findByNameOneResult("Chelsea");
        System.out.println(arsenal.getTeamDetailsList().size() + "ars size");
        System.out.println(chelsea.getTeamDetailsList().size() + " chel size");

        //String score = footballUtils.CalculateScoreForMatch(teams.get(0), teams2.get(0), 1, 2021);
        //System.out.println(score);
    }


    @GetMapping("/testLeague")
    public void testInitLeague() {
        leagueService.initLeaguePerSeason(2021);
    }

    @GetMapping("/testEndSeason")
    public void testEndSeason(){
        boolean ended = tournamentService.verifySeasonEndAcrossGlobe();
        if(ended){
            System.out.println("All seasons are over !");
        }
    }

    @GetMapping("/testRound")
    public void testRound() {

            List<Round> roundList = tournamentService.initializeRound();
            for (Round round : roundList) {
                Tournament tournament = tournamentService.findById(round.getTournament().getId());
                if (tournament != null) {
                    League league = leagueService.findById(tournament.getLeague().getId());
                    if (league != null) {
                        log.info("Pentru " + league.getName() + " se genereaza runda cu numarul " + round.getRoundNumber());
                        //System.out.println("s-a generat pentru liga : " + league.getName() + " cu turneul "+ tournament.getYear() + " runda " + round.getRoundNumber());
                        footballUtils.GenerateMatchPerRound(league.getTeamList(), tournament, round);
                    }
                }
            }
            List<Tournament> tournamentList = tournamentService.verifySeasonEnd();
            if (tournamentList.size() != 0) {
                for (Tournament t : tournamentList) {
                    log.info("Tournament " + t.getLeague().getName() + " has ended");
                }
            } else {
                System.out.println("gol 5");
            }
    }

    @GetMapping("/testExport")
    public void testExportDataList(@RequestParam("name") String name, @RequestParam("year") int year){
        List<FootballData> footballDataList = footballDataExportService.getFootballDataWithLeague(name, year);
        for(FootballData footballData : footballDataList){
            System.out.println("In " + footballData.getLeagueName() + " Runda " + footballData.getRoundID()+ " au jucat: " + footballData.getNameTeam1() + " si " +
                    footballData.getNameTeam2() + " cu scorul " + footballData.getScore() + " in formatiile " + footballData.getFormationTeam1() +
                    " si " + footballData.getFormationTeam2());
        }
    }


    @GetMapping("/exportExcel")
    public void exportToExcel(HttpServletResponse response) throws IOException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());

        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=leagueResults_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);


        List<FootballData> footballDataList = footballDataExportService.getFootballDataWithLeague("Premier League", 2021);

        ExcelExporter excelExporter = new ExcelExporter(footballDataList);

        excelExporter.export(response);
    }

    @GetMapping("/sendMailTemplate")
    public void sendMailTemplate(){
        League league = leagueService.findByNameOneResult("Premier League");
        Tournament t = null;
        if(league.getTournamentList().size()!=0){
            List<Tournament> tournamentList = league.getTournamentList();
            for(Tournament tournament : tournamentList){
                if(tournament.getOngoing()){
                    t = tournament;
                    break;
                }
            }
        }
        Round r1 = null;
        if(t!=null){
            if(t.getRoundList().size()!=0){
                List<Round> roundList = t.getRoundList();
                r1 = roundList.get(0);
            }
        }
        List<User> users = userService.findByPreference("Premier League");
        if(users.size()!=0 && r1 != null){
            for(User user : users){
                /*emailService.constructRoundNotificationEmail()*/
            }
        }
    }
}
