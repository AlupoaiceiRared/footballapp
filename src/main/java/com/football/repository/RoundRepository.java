package com.football.repository;

import com.football.domain.Round;
import com.football.domain.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoundRepository extends CrudRepository<Round, Integer> {


    @Query(value = "SELECT r from Round r WHERE r.RoundNumber = ?1 AND r.year = ?2 AND r.tournament.id = ?3")
    Round findByRoundNumberAndYearAndTournament(int roundNumber, int year, int tournamentId);

}
