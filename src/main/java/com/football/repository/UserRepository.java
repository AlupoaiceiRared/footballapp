package com.football.repository;

import com.football.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    List<User> findByLastnameLike(String lastname);

    @Query(value = "SELECT u from User u WHERE u.leaguePreference = ?1")
    List<User> findByPreference(String preference);
}
