package com.football.repository;

import com.football.domain.TeamDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamDetailsRepository extends CrudRepository<TeamDetails, Integer> {

    @Query(value = "SELECT td from TeamDetails td WHERE td.round = ?1 AND td.year = ?2")
    TeamDetails findByRoundAndYear(int roundNumber, int year);

    @Query(value = "SELECT td from TeamDetails td WHERE td.round = ?1 AND td.year = ?2 AND td.team.id = ?3")
    TeamDetails findByRoundAndYearAndTeam(int roundNumber, int year, int teamId);

}
