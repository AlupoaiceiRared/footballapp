package com.football.repository;

import com.football.domain.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQuery;
import java.util.List;

@Repository
public interface TeamRepository extends CrudRepository<Team, Integer> {

    List<Team> findByNameLike(String name);

    @Query(value = "SELECT t from Team t WHERE t.name = ?1")
    Team findByNameOneResult(String name);
}
