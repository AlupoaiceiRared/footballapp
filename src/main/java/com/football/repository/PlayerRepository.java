package com.football.repository;

import com.football.domain.Player;
import com.football.domain.Team;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlayerRepository extends CrudRepository<Player, Integer> {


    List<Player> findByLastnameLike(String name);

    List<Player> findByFirstnameLike(String name);

    @Query(value = "SELECT p from Player p WHERE p.firstname = ?1 AND p.lastname = ?2 AND p.position = ?3")
    Player findByFirstnameLastnamePosition(String firstname, String lastname, String position);
}
