package com.football.export;

import com.football.domain.*;
import com.football.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class FootballDataExportService {

    @Autowired
    private TeamService teamService;

    @Autowired
    private TeamDetailsService teamDetailsService;

    @Autowired
    private LeagueService leagueService;

    @Autowired
    private RoundService roundService;

    @Autowired
    private TournamentService tournamentService;


    public List<FootballData> getFootballDataWithLeague(String name, int year){
        List<FootballData> footballDataList = new ArrayList<>();
        League league = leagueService.findByNameOneResult(name);
        if(league != null){
            if(league.getTournamentList().size() != 0){
                List<Tournament> tournamentList = league.getTournamentList();
                Tournament tournament = tournamentService.findTournamentFromLeagueWithYear(year,tournamentList);
                if(tournament != null) {
                    if(tournament.getRoundList().size() != 0){
                        List<Round> roundList = tournament.getRoundList();
                        for(Round round : roundList){
                            if(round.getMatches().size() != 0){
                                List<Match> matchesPerRound = round.getMatches();
                                for(Match match : matchesPerRound){
                                    String teamString1 = match.getTeam1();
                                    String teamString2 = match.getTeam2();
                                    int scoreTeam1 = match.getScoreT1();
                                    int scoreTeam2 = match.getScoreT2();
                                    String score = String.valueOf(scoreTeam1) + "-" + String.valueOf(scoreTeam2);
                                    Team team1 = teamService.findByNameOneResult(teamString1);
                                    Team team2 = teamService.findByNameOneResult(teamString2);
                                    TeamDetails teamDetails1 = teamDetailsService.findByYearRoundAndTeamId(round.getRoundNumber(), year, team1.getId());
                                    TeamDetails teamDetails2 = teamDetailsService.findByYearRoundAndTeamId(round.getRoundNumber(), year, team2.getId());

                                    FootballData footballData = new FootballData(round.getRoundNumber(),league.getName(),teamString1,teamString2, score, teamDetails1.getFormation(), teamDetails2.getFormation());
                                    footballDataList.add(footballData);
                                }
                            }
                        }
                    }
                }
            }
        }
        return footballDataList;
    }
}
