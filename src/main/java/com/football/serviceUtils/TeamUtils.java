package com.football.serviceUtils;

import com.football.domain.Formation;
import com.football.domain.Player;
import com.football.domain.Team;
import com.football.service.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

@Service
public class TeamUtils {


    @Autowired
    private PlayerService playerService;

    List<String> attPositions = Arrays.asList("ST", "RW", "LW", "CF", "RF", "LF");
    List<String> midPositions = Arrays.asList("CAM", "CM", "RM", "LM", "CDM");
    List<String> defPositions = Arrays.asList("CB", "RB", "LB", "RB", "LB", "RWB", "LWB");

    public HashMap<String,List<Player>> TeamBuild(Formation formation, Team team) {
        List<Player> goalkeepers = new ArrayList<>();
        List<Player> defenders = new ArrayList<>();
        List<Player> midfielders = new ArrayList<>();
        List<Player> attackers = new ArrayList<>();
        List<Player> substitutes = new ArrayList<>();

        int l = 0, j = 0, k = 0;

        HashMap<String, List<Player>> formationMap = new HashMap<>();

        List<Player> finalListOfGoalkeepers = new ArrayList<>();
        List<Player> finalListOfDefenders = new ArrayList<>();
        List<Player> finalListOfMidfielders = new ArrayList<>();
        List<Player> finalListOfAttackers = new ArrayList<>();

        switch (formation){
            case F433: {
                List<Player> allPlayers433 = team.getPlayerList();
                for (Player p : allPlayers433) {
                    if (p.getPosition().equalsIgnoreCase("GK")) {
                        goalkeepers.add(p);
                    }
                    if (attPositions.contains(p.getPosition())) {
                        attackers.add(p);
                    }
                    if (midPositions.contains(p.getPosition())) {
                        midfielders.add(p);
                    }
                    if (defPositions.contains(p.getPosition())) {
                        defenders.add(p);
                    }
                }
                goalkeepers = generateRatingForGoalkeepers(goalkeepers);
                defenders = generateRatingForDefenders(defenders);
                midfielders = generateRatingForMidfielders(midfielders);
                attackers = generateRatingForAttackers(attackers);

                //TODO check for size (all arrays)
                goalkeepers.sort(Comparator.comparing(Player::getRating).reversed());
                defenders.sort(Comparator.comparing(Player::getRating).reversed());
                midfielders.sort(Comparator.comparing(Player::getRating).reversed());
                attackers.sort(Comparator.comparing(Player::getRating).reversed());

                finalListOfGoalkeepers.add(goalkeepers.get(0));
                if(goalkeepers.size()>1){
                    substitutes.add(goalkeepers.get(1));
                }

                for (int i = 0; i < 4; i++) {
                    if (i == 0 || i == 1 || i == 2) {
                        finalListOfAttackers.add(attackers.get(i));
                        finalListOfMidfielders.add(midfielders.get(i));
                        finalListOfDefenders.add(defenders.get(i));
                    }
                    if (i == 3) {
                        finalListOfDefenders.add(defenders.get(i));
                    }
                }

                l = 4;
                j = 3;
                k = 3;
                substitutes = addToSubstitutes(l, defenders, substitutes);
                substitutes = addToSubstitutes(j, midfielders, substitutes);
                substitutes = addToSubstitutes(k, attackers, substitutes);


                formationMap.put("GK", finalListOfGoalkeepers);
                formationMap.put("DEF", finalListOfDefenders);
                formationMap.put("MID", finalListOfMidfielders);
                formationMap.put("ATT", finalListOfAttackers);
                formationMap.put("SUB", substitutes);

                return formationMap;
            }
            case F442: {
                List<Player> allPlayers442 = team.getPlayerList();
                for (Player p : allPlayers442) {
                    if (p.getPosition().equalsIgnoreCase("GK")) {
                        goalkeepers.add(p);
                    }
                    if (attPositions.contains(p.getPosition())) {
                        attackers.add(p);
                    }
                    if (midPositions.contains(p.getPosition())) {
                        midfielders.add(p);
                    }
                    if (defPositions.contains(p.getPosition())) {
                        defenders.add(p);
                    }
                }

                goalkeepers = generateRatingForGoalkeepers(goalkeepers);
                defenders = generateRatingForDefenders(defenders);
                midfielders = generateRatingForMidfielders(midfielders);
                attackers = generateRatingForAttackers(attackers);

                //TODO check for size (all arrays)
                goalkeepers.sort(Comparator.comparing(Player::getRating).reversed());
                defenders.sort(Comparator.comparing(Player::getRating).reversed());
                midfielders.sort(Comparator.comparing(Player::getRating).reversed());
                attackers.sort(Comparator.comparing(Player::getRating).reversed());
                finalListOfGoalkeepers.add(goalkeepers.get(0));
                if(goalkeepers.size()>1){
                    substitutes.add(goalkeepers.get(1));
                }



                for (int i = 0; i < 4; i++) {
                    if (i == 0 || i == 1) {
                        finalListOfMidfielders.add(midfielders.get(i));
                        finalListOfDefenders.add(defenders.get(i));
                        finalListOfAttackers.add(attackers.get(i));
                    }
                    if (i == 2 || i == 3) {
                        finalListOfDefenders.add(defenders.get(i));
                        finalListOfMidfielders.add(midfielders.get(i));
                    }
                }

                l = 4;
                j = 4;
                k = 2;
                substitutes = addToSubstitutes(l, defenders, substitutes);
                substitutes = addToSubstitutes(j, midfielders, substitutes);
                substitutes = addToSubstitutes(k, attackers, substitutes);


                formationMap.put("GK", finalListOfGoalkeepers);
                formationMap.put("DEF", finalListOfDefenders);
                formationMap.put("MID", finalListOfMidfielders);
                formationMap.put("ATT", finalListOfAttackers);
                formationMap.put("SUB", substitutes);

                return formationMap;
            }
            case F541 : {
                List<Player> allPlayers541 = team.getPlayerList();
                for (Player p : allPlayers541) {
                    if (p.getPosition().equalsIgnoreCase("GK")) {
                        goalkeepers.add(p);
                    }
                    if (attPositions.contains(p.getPosition())) {
                        attackers.add(p);
                    }
                    if (midPositions.contains(p.getPosition())) {
                        midfielders.add(p);
                    }
                    if (defPositions.contains(p.getPosition())) {
                        defenders.add(p);
                    }
                }

                goalkeepers = generateRatingForGoalkeepers(goalkeepers);
                defenders = generateRatingForDefenders(defenders);
                midfielders = generateRatingForMidfielders(midfielders);
                attackers = generateRatingForAttackers(attackers);

                //TODO check for size (all arrays)
                goalkeepers.sort(Comparator.comparing(Player::getRating).reversed());
                defenders.sort(Comparator.comparing(Player::getRating).reversed());
                midfielders.sort(Comparator.comparing(Player::getRating).reversed());
                attackers.sort(Comparator.comparing(Player::getRating).reversed());
                finalListOfGoalkeepers.add(goalkeepers.get(0));
                if(goalkeepers.size()>1){
                    substitutes.add(goalkeepers.get(1));
                }

                finalListOfAttackers.add(attackers.get(0));

                for (int i = 0; i < 5; i++) {
                    if (i != 4) {
                        finalListOfMidfielders.add(midfielders.get(i));
                        finalListOfDefenders.add(defenders.get(i));
                    }
                    if (i == 4) {
                        finalListOfDefenders.add(defenders.get(i));
                    }
                }

                l = 5;
                j = 4;
                k = 1;
                substitutes = addToSubstitutes(l, defenders, substitutes);
                substitutes = addToSubstitutes(j, midfielders, substitutes);
                substitutes = addToSubstitutes(k, attackers, substitutes);


                formationMap.put("GK", finalListOfGoalkeepers);
                formationMap.put("DEF", finalListOfDefenders);
                formationMap.put("MID", finalListOfMidfielders);
                formationMap.put("ATT", finalListOfAttackers);
                formationMap.put("SUB", substitutes);

                return formationMap;
            }
        }


    return null;
    }



    public float generateRandomFloatWithOneDigit(){
        Random rand = new Random();
        int number = rand.nextInt(10);
        if(number>5){
            number -=5;
        }
        Float floatNr = new Float(number);
        floatNr /=10;
        return floatNr;
    }

    public List<Player> addToSubstitutes(int i, List<Player> playerList, List<Player> subs){
        if(i != playerList.size()) {
            while (i < playerList.size()) {
                int nrOfPlayers = 0;
                subs.add(playerList.get(i));
                nrOfPlayers++;
                if (nrOfPlayers == 2) {
                    break;
                } else {
                    i++;
                }
            }
        }
        return subs;
    }


    public List<Player> generateRatingForGoalkeepers(List<Player> goalkeepers){
        for(Player goalkeeper : goalkeepers){
            float randomFloat = generateRandomFloatWithOneDigit();
            float rating;
            int intNumber = (int) (randomFloat*10);
            if(intNumber % 2 ==0) {
                rating = (goalkeeper.getGoalkeeping() * 6 + goalkeeper.getDefending() * 3 + goalkeeper.getPassing() * 1 + randomFloat) / 10;
            } else {
                rating = (goalkeeper.getGoalkeeping() * 6 + goalkeeper.getDefending() * 3 + goalkeeper.getPassing() * 1 - randomFloat) / 10;
            }
            if(rating > 10f){
                rating = 9.9f;
            } else{
                rating = OneDigitFloat(rating);
            }
            goalkeeper.setRating(rating);
            playerService.updatePlayer(goalkeeper);

        }
        return goalkeepers;
    }

    public List<Player> generateRatingForDefenders(List<Player> defenders){
        for(Player defender : defenders){
            float randomFloat = generateRandomFloatWithOneDigit();
            float rating;
            int intNumber = (int) (randomFloat*10);
            if(intNumber % 2 ==0) {
                rating = (defender.getDefending() * 6 + defender.getPassing() * 3 + defender.getSpeed() * 1 ) / 10 + randomFloat;
            } else {
                rating = (defender.getDefending() * 6 + defender.getPassing() * 3 + defender.getSpeed() * 1 ) / 10 - randomFloat;
            }
            if(rating > 10f){
                rating = 9.9f;
            } else {
                rating = OneDigitFloat(rating);
            }
            defender.setRating(rating);
            playerService.updatePlayer(defender);
        }
        return defenders;
    }

    public List<Player> generateRatingForMidfielders(List<Player> midfielders){
        for(Player midfielder : midfielders){
            float randomFloat = generateRandomFloatWithOneDigit();
            float rating;
            int intNumber = (int) (randomFloat*10);
            if(intNumber % 2 ==0) {
                rating = (midfielder.getPassing() * 4 + midfielder.getSpeed() * 3 + midfielder.getShooting() * 2 + midfielder.getDefending() * 1 + randomFloat) / 10;
            } else {
                rating = (midfielder.getPassing() * 4 + midfielder.getSpeed() * 3 + midfielder.getShooting() * 2 + midfielder.getDefending() * 1 - randomFloat) / 10;
            }
            if(rating > 10f){
                rating = 9.9f;
            } else {
                rating = OneDigitFloat(rating);
            }
            midfielder.setRating(rating);
            playerService.updatePlayer(midfielder);
        }
        return midfielders;
    }

    public List<Player> generateRatingForAttackers(List<Player> attackers){
        for(Player attacker : attackers){
            float randomFloat = generateRandomFloatWithOneDigit();
            float rating;
            int intNumber = (int) (randomFloat*10);
            if(intNumber % 2 ==0) {
                rating = (attacker.getShooting() * 5 + attacker.getSpeed() * 3 + attacker.getPassing() * 2 + randomFloat) / 10;
            } else {
                rating = (attacker.getShooting() * 5 + attacker.getSpeed() * 3 + attacker.getPassing() * 2 - randomFloat) / 10;
            }
            if(rating > 10f){
                rating = 9.9f;
            } else {
                rating = OneDigitFloat(rating);
            }
            attacker.setRating(rating);
            playerService.updatePlayer(attacker);
        }
        return attackers;
    }

    public float OneDigitFloat(float number){
        DecimalFormat df = new DecimalFormat("#.#");
        df.setRoundingMode(RoundingMode.CEILING);
        float float_random=Float.parseFloat(df.format(number));
        return float_random;
    }


    Comparator<Player> ratingComparator = (p1, p2) -> {
        int result = Float.compare(p1.getRating(), p1.getRating());
        return result;
    };
}
