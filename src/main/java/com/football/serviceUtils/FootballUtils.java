package com.football.serviceUtils;

import com.football.domain.*;
import com.football.mail.EmailService;
import com.football.service.*;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.record.pivottable.StreamIDRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class FootballUtils {

    @Autowired
    private TeamService teamService;

    @Autowired
    private MatchService matchService;

    @Autowired
    private TeamDetailsService teamDetailsService;

    @Autowired
    private RoundService roundService;

    @Autowired
    private TournamentService tournamentService;

    @Autowired
    private TeamUtils teamUtils;

    @Autowired
    private UserService userService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private JavaMailSender javaMailSender;

    public void GenerateMatchPerRound(List<Team> teamList, Tournament tournament, Round round){
        teamList.sort(Comparator.comparing(Team::getName));
        List<String> listOfTeamNames = new ArrayList<>();
        for(Team team:teamList){
            listOfTeamNames.add(team.getName());
        }

        roundSimulation(listOfTeamNames,round, tournament);
        tournamentService.save(tournament);


    }

    public void roundSimulation(List<String> teamList, Round round,Tournament tournament)
    {

        if (teamList.size() % 2 != 0)
        {
            System.out.println("impar");
        } else {

            HashMap<Team, Team> matchdayFormat = new HashMap<>();

            int halfSize = teamList.size() / 2;

            List<String> listOfTeamsNames = new ArrayList<>(teamList);
            // Add teams to List and remove the first team
            //removing arsenal
            listOfTeamsNames.remove(0);
            int listOfTeamsNamesSize = listOfTeamsNames.size();

            int etapa = round.getRoundNumber() - 1;
            //increment etapa pentru afisaj
            etapa = etapa + 1;

            if (etapa < 20) {
                for (int day = etapa - 1; day < etapa; day++) {
                    System.out.println("ETAPA: " + etapa + "/" + round.getYear());

                    int teamIdx = day % listOfTeamsNamesSize;
                    //AV vs Arsenal
                    System.out.println((listOfTeamsNames.get(teamIdx) + "(" + teamIdx + ") " + " -vs-  " + teamList.get(0) + "(0)"));
                    matchdayFormat.put(teamService.findByNameOneResult(listOfTeamsNames.get(teamIdx)), teamService.findByNameOneResult(teamList.get(0)));

                    for (int idx = 1; idx < halfSize; idx++) {

                        int firstTeam = (day + idx) % listOfTeamsNamesSize;
                        int secondTeam = (day + listOfTeamsNamesSize - idx) % listOfTeamsNamesSize;

                        System.out.println(listOfTeamsNames.get(firstTeam) + "(" + firstTeam + ") " + " vs " + listOfTeamsNames.get(secondTeam) + "(" + secondTeam + ") ");
                        matchdayFormat.put(teamService.findByNameOneResult(listOfTeamsNames.get(firstTeam)), teamService.findByNameOneResult(listOfTeamsNames.get(secondTeam)));

                    }
                }
            } else if (etapa >= 20) {
                for (int day = etapa - 20; day < etapa - 19; day++) {

                    System.out.println("ETAPA: " + etapa);
                    int teamIdx = day % listOfTeamsNamesSize;
                    System.out.println((listOfTeamsNames.get(teamIdx) + " -vs-  " + teamList.get(0)));
                    if (verifyMatchAwayOrHome(teamService.findByNameOneResult(listOfTeamsNames.get(teamIdx)), teamService.findByNameOneResult(teamList.get(0)), tournament)) {
                        matchdayFormat.put(teamService.findByNameOneResult(teamList.get(0)), teamService.findByNameOneResult(listOfTeamsNames.get(teamIdx)));
                    } else {
                        matchdayFormat.put(teamService.findByNameOneResult(listOfTeamsNames.get(teamIdx)), teamService.findByNameOneResult(teamList.get(0)));
                    }

                    for (int idx = 1; idx < halfSize; idx++) {
                        int firstTeam = (day + idx) % listOfTeamsNamesSize;
                        int secondTeam = (day + listOfTeamsNamesSize - idx) % listOfTeamsNamesSize;
                        System.out.println(listOfTeamsNames.get(firstTeam) + " vs " + listOfTeamsNames.get(secondTeam));
                        if (verifyMatchAwayOrHome(teamService.findByNameOneResult(listOfTeamsNames.get(firstTeam)), teamService.findByNameOneResult(listOfTeamsNames.get(secondTeam)), tournament)) {
                            matchdayFormat.put(teamService.findByNameOneResult(listOfTeamsNames.get(secondTeam)), teamService.findByNameOneResult(listOfTeamsNames.get(firstTeam)));
                        } else {
                            matchdayFormat.put(teamService.findByNameOneResult(listOfTeamsNames.get(firstTeam)), teamService.findByNameOneResult(listOfTeamsNames.get(secondTeam)));
                        }
                    }
                }
            }

            for (Map.Entry<Team, Team> entry : matchdayFormat.entrySet()) {
                System.out.println(entry.getKey().getName() + " - " + entry.getValue().getName());
                Match match = new Match();
                matchService.CreateMatch(match);
                matchService.UpdateMatch(match, entry.getKey(), entry.getValue());
                if (round.getMatches().size() == 0) {
                    List<Match> matches = new ArrayList<>();
                    matches.add(match);
                    round.setMatches(matches);
                    roundService.saveRound(round);
                    HashMap<String, List<Player>> mapt1 = initializeRandomTeamDetailsForTeam(entry.getKey(), round);
                    HashMap<String, List<Player>> mapt2 = initializeRandomTeamDetailsForTeam(entry.getValue(), round);
                    String score = CalculateScoreForMatch(entry.getKey(), entry.getValue(), mapt1, mapt2, round.getRoundNumber(), round.getYear());
                    int indexOfSign = score.indexOf("-");
                    int scoreTeam1 = Integer.parseInt(score.substring(0, indexOfSign));
                    int scoreTeam2 = Integer.parseInt(score.substring(indexOfSign + 1, score.length()));
                    match.setScoreT1(scoreTeam1);
                    match.setScoreT2(scoreTeam2);
                    matchService.UpdateMatchRound(match, round);
                    calculatePointAfterMatch(entry.getKey(),entry.getValue(),score);

                } else {
                    round.getMatches().add(match);
                    roundService.saveRound(round);
                    HashMap<String, List<Player>> mapt1 = initializeRandomTeamDetailsForTeam(entry.getKey(), round);
                    HashMap<String, List<Player>> mapt2 = initializeRandomTeamDetailsForTeam(entry.getValue(), round);
                    String score = CalculateScoreForMatch(entry.getKey(), entry.getValue(), mapt1, mapt2, round.getRoundNumber(), round.getYear());
                    int indexOfSign = score.indexOf("-");
                    int scoreTeam1 = Integer.parseInt(score.substring(0, indexOfSign));
                    int scoreTeam2 = Integer.parseInt(score.substring(indexOfSign + 1, score.length()));
                    match.setScoreT1(scoreTeam1);
                    match.setScoreT2(scoreTeam2);
                    matchService.UpdateMatchRound(match, round);
                    calculatePointAfterMatch(entry.getKey(),entry.getValue(),score);
                }

            }

            List<Team> leagueTable = tournament.getLeague().getTeamList();
            leagueTable.sort(Comparator.comparing(Team::getPoints).reversed());
            List<User> users = userService.findByPreference(tournament.getLeague().getName());
            if(users.size()!=0){
                for(User user:users){
                    javaMailSender.send(emailService.constructRoundNotificationEmail(user,round,round.getMatches(),tournament.getLeague().getName(), leagueTable));
                }
            }
        }
    }

    public HashMap<String, List<Player>> initializeRandomTeamDetailsForTeam(Team team1, Round round){
        Random rand = new Random();
        int randomNr = rand.nextInt();
        Formation f = Formation.F433;
        if(randomNr % 3 == 0){
            f = Formation.F541;
        } else if(randomNr % 3 == 2){
            f = Formation.F442;
        }

        HashMap<String, List<Player>> mapF = teamUtils.TeamBuild(f,team1);
        TeamDetails td1 = new TeamDetails();
        td1.setRound(round.getRoundNumber());
        td1.setTeam(team1);
        td1.setYear(round.getYear());
        td1.setConfigurationPerMatchMap(mapF);
        td1.setFormation(f.getFormation());
        teamDetailsService.save(td1);

        return mapF;

    }

    public Boolean verifyMatchAwayOrHome(Team t1, Team t2, Tournament tournament){
        List<Match> matches = matchService.findByBothTeams(t1.getName(), t2.getName());
        List<Round> roundListFromTournament = tournament.getRoundList();
        for(Round round : roundListFromTournament){
            for(Match match : round.getMatches()){
                for(Match m : matches){
                    if(m.equals(match)){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public String CalculateScoreForMatch(Team team1, Team team2, HashMap<String, List<Player>> mapt1, HashMap<String, List<Player>> mapt2, int round, int year){
        List<TeamDetails> teamDetailsListTeam1 = team1.getTeamDetailsList();
        List<TeamDetails> teamDetailsListTeam2 = team2.getTeamDetailsList();
        TeamDetails teamDetails1 = new TeamDetails();
        TeamDetails teamDetails2 = new TeamDetails();

        float gkScoreTeam1 = 0; float gkScoreTeam2 = 0;
        float defScoreTeam1 = 0; float defScoreTeam2 = 0;
        float midScoreTeam1 = 0; float midScoreTeam2 = 0;
        float attScoreTeam1 = 0; float attScoreTeam2 = 0;


        for(TeamDetails teamDetails : teamDetailsListTeam1){
            if(teamDetails.getRound() == round && teamDetails.getYear() == year){
                teamDetails1 = teamDetails;
                System.out.printf("dada");
                System.out.println(teamDetails1.toString());
                break;
            }
        }
        for(TeamDetails teamDetails : teamDetailsListTeam2){
            if(teamDetails.getRound() == round && teamDetails.getYear() == year){
                teamDetails2 = teamDetails;
                break;
            }
        }

        HashMap<String,List<Player>> fieldMapTeam1 = mapt1;//teamDetails1.getConfigurationPerMatchMap();

        HashMap<String,List<Player>> fieldMapTeam2 = mapt2;//teamDetails2.getConfigurationPerMatchMap();

        for (Map.Entry<String, List<Player>> entry : fieldMapTeam1.entrySet()) {
            for(Player player : entry.getValue()){
                if(entry.getKey().equalsIgnoreCase("GK")){
                    gkScoreTeam1 += player.getRating();
                }
                if(entry.getKey().equalsIgnoreCase("MID")){
                    midScoreTeam1 += player.getRating();
                }
                if(entry.getKey().equalsIgnoreCase("DEF")){
                    defScoreTeam1 += player.getRating();
                }
                if(entry.getKey().equalsIgnoreCase("ATT")){
                    attScoreTeam1 += player.getRating();
                }
            }
        }

        for (Map.Entry<String, List<Player>> entry : fieldMapTeam2.entrySet()) {
            for(Player player : entry.getValue()){
                if(entry.getKey().equalsIgnoreCase("GK")){
                    gkScoreTeam2 += player.getRating();
                }
                if(entry.getKey().equalsIgnoreCase("MID")){
                    midScoreTeam2 += player.getRating();
                }
                if(entry.getKey().equalsIgnoreCase("DEF")){
                    defScoreTeam2 += player.getRating();
                }
                if(entry.getKey().equalsIgnoreCase("ATT")){
                    attScoreTeam2 += player.getRating();
                }
            }
        }

        int goalsTeam1=0; int goalsTeam2=0;

        //-54 < -50
        if((3.0*gkScoreTeam1 + 6.0*defScoreTeam1 + 1.0*midScoreTeam1) - (4.0*midScoreTeam2 + 6.0*attScoreTeam2)  < -50){
            Random r = new Random();
            goalsTeam2 = 4 + r.nextInt(3);
        } else if((3.0*gkScoreTeam1 + 6.0*defScoreTeam1 + 1.0*midScoreTeam1) - (4.0*midScoreTeam2 + 6.0*attScoreTeam2)  < -30){
            Random r = new Random();
            goalsTeam2 = 1 + r.nextInt(3);
        } else if((3.0*gkScoreTeam1 + 6.0*defScoreTeam1 + 1.0*midScoreTeam1) - (4.0*midScoreTeam2 + 6.0*attScoreTeam2)  < 0){
            Random r = new Random();
            goalsTeam2 = 1 + r.nextInt(2);
        }

        if((3.0*gkScoreTeam2 + 6.0*defScoreTeam2 + 1.0*midScoreTeam2) - (4.0*midScoreTeam1 + 6.0*attScoreTeam1)  < -50){
            Random r = new Random();
            goalsTeam1 = 4 + r.nextInt(3);
        } else if((3.0*gkScoreTeam2 + 6.0*defScoreTeam2 + 1.0*midScoreTeam2) - (4.0*midScoreTeam1 + 6.0*attScoreTeam1)  < -30){
            Random r = new Random();
            goalsTeam1 = 1 + r.nextInt(3);
        } else if((3.0*gkScoreTeam2 + 6.0*defScoreTeam2 + 1.0*midScoreTeam2) - (4.0*midScoreTeam1 + 6.0*attScoreTeam1)  < 0){
            Random r = new Random();
            goalsTeam1 = 1 + r.nextInt(2);
        }

        if(goalsTeam1 == 0){
            float number = defScoreTeam2 - attScoreTeam1 - midScoreTeam1;
            if(number < 0) {
                number = (number < 0 ? -number : 0);
                if (number > gkScoreTeam2) {
                    goalsTeam1 += 1;
                }
            }
        }

        if(goalsTeam2 == 0){
            float number = defScoreTeam1 - attScoreTeam2 - midScoreTeam2;
            if(number < 0) {
                number = (number < 0 ? -number : 0);
                if (number > gkScoreTeam1) {
                    goalsTeam2 += 1;
                }
            }
        }

        String score = String.valueOf(goalsTeam1) + "-" + String.valueOf(goalsTeam2);
        log.info("Generated score " + score + " for " + team1.getName() + " vs " + team2.getName());
        log.info("Stats for " + team1.getName() + " are:  Formation - " + teamDetails1.getFormation() + "  GK - " + gkScoreTeam1 + "  DEF - " + defScoreTeam1 + "  MID - " + midScoreTeam1 + "  ATT - " + attScoreTeam1);
        log.info("Stats for " + team2.getName() + " are:  Formation - " + teamDetails2.getFormation() + "  GK - " + gkScoreTeam2 + "  DEF - " + defScoreTeam2 + "  MID - " + midScoreTeam2 + "  ATT - " + attScoreTeam2);
        return score;

    }

    public void calculatePointAfterMatch(Team team1, Team team2, String score){
        int indexOfSign = score.indexOf("-");
        int scoreTeam1 = Integer.parseInt(score.substring(0, indexOfSign));
        int scoreTeam2 = Integer.parseInt(score.substring(indexOfSign + 1, score.length()));

        if(scoreTeam1 > scoreTeam2){
            team1.setPoints(team1.getPoints()+3);
        } else if(scoreTeam1 < scoreTeam2){
            team2.setPoints(team2.getPoints() + 3);
        } else {
            team1.setPoints(team1.getPoints() + 1);
            team2.setPoints(team2.getPoints() + 1);
        }

        teamService.UpdateTeam(team1);
        teamService.UpdateTeam(team2);
    }







    public void ListMatches(List<String> teamList,String[][] leagueTable, List<Integer> setOfNumbers, HashMap<Integer, String> teamsMap, int etapa )
    {
        if (teamList.size() % 2 != 0)
        {
            System.out.println("impar"); // If odd number of teams add a dummy
        }

        int numDays = (teamList.size() - 1); // Days needed to complete tournament
        int halfSize = teamList.size() / 2;



        List<String> teams = new ArrayList<>(teamList);
        // Add teams to List and remove the first team
        teams.remove(0);

        int teamsSize = teams.size();

        for (int day = 0; day < numDays; day++)
        {
            System.out.println("Day {0}" + (day + 1) + "teamSize este " + teamsSize);

            int teamIdx = day % teamsSize;

            System.out.println(("{0} vs {1}" + teams.get(teamIdx) +" " + teamList.get(0)));

            for (int idx = 1; idx < halfSize; idx++)
            {
                int firstTeam = (day + idx) % teamsSize;
                System.out.println("day " + day + "  idx " + idx + "    teamSize "+ teamsSize);
                int secondTeam = (day  + teamsSize - idx) % teamsSize;
                System.out.println("day " + day + "  idx " + idx + "    teamSize "+ teamsSize);
                System.out.println("{r} vs {r} " +  teams.get(firstTeam) + " " + teams.get(secondTeam));
            }
        }
    }
}