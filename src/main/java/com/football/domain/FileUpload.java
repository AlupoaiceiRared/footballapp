package com.football.domain;

import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;

@Entity
public class FileUpload {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "FILE_FILENAME")
    private String filename;
    @Column(name = "FILE_PATH")
    private String path;
    @Column(name = "FILE_UPLOAD_DATE")
    private String uploadDate;
    @Column(name = "FILE_MONTH")
    private Integer monthOfTheFile;
    @Column(name = "FILE_YEAR")
    private Integer yearOfTheFile;

    @Transient
    private MultipartFile filedata;


    public FileUpload(String filename, String path, String uploadDate, Integer monthOfTheReport, Integer yearOfTheReport, MultipartFile filedata) {
        this.filename = filename;
        this.path = path;
        this.uploadDate = uploadDate;
        this.monthOfTheFile = monthOfTheReport;
        this.monthOfTheFile = yearOfTheReport;
        this.filedata = filedata;
    }

    public FileUpload() {
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUploadDate() {
        return uploadDate;
    }

    public void setUploadDate(String uploadDate) {
        this.uploadDate = uploadDate;
    }

    public Integer getMonthOfTheFile() {
        return monthOfTheFile;
    }

    public void setMonthOfTheFile(Integer monthOfTheFile) {
        this.monthOfTheFile = monthOfTheFile;
    }

    public Integer getYearOfTheFile() {
        return yearOfTheFile;
    }

    public void setYearOfTheFile(Integer yearOfTheFile) {
        this.yearOfTheFile = yearOfTheFile;
    }

    public MultipartFile getFiledata() {
        return filedata;
    }

    public void setFiledata(MultipartFile filedata) {
        this.filedata = filedata;
    }
}
