package com.football.domain;

public enum Formation {

    F433 ("4-3-3"),
    F442 ("4-4-2"),
    F541 ("5-4-1");

    private final String value;

    private Formation (final String value){
        this.value = value;
    }

    public String getFormation(){
        return value;
    }
}