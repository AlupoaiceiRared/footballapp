package com.football.domain;

import javax.persistence.*;
import java.util.HashMap;
import java.util.List;

@Entity
public class TeamDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Transient
    private HashMap<String, List<Player>> configurationPerMatchMap;

    private int round;

    private int year;

    @ManyToOne
    @JoinColumn(name="team_id")
    private Team team;

    private String formation;

    public TeamDetails(){

    }

    public TeamDetails(TeamDetails teamDetails){
        this.round = teamDetails.round;
        this.year = teamDetails.year;
        this.configurationPerMatchMap = teamDetails.configurationPerMatchMap;
        this.team = teamDetails.team;
        this.formation = teamDetails.formation;
    }

    public HashMap<String, List<Player>> getConfigurationPerMatchMap() {
        return configurationPerMatchMap;
    }

    public void setConfigurationPerMatchMap(HashMap<String, List<Player>> configurationPerMatchMap) {
        this.configurationPerMatchMap = configurationPerMatchMap;
    }

    public int getRound() {
        return round;
    }

    public void setRound(int round) {
        this.round = round;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public String getFormation() {
        return formation;
    }

    public void setFormation(String formation) {
        this.formation = formation;
    }
}
