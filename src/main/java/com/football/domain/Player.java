package com.football.domain;

import javax.persistence.*;

@Entity
public class Player {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private int age;

    private String firstname;

    private String lastname;

    private String position;

    @ManyToOne
    @JoinColumn(name="team_id")
    private Team team;

    private float defending;

    private float goalkeeping;

    private Boolean injured;

    private float passing;

    private float shooting;

    private float speed;

    private Boolean suspended;

    private float rating;



    public Player() {
    }

    public Player(String firstname, String lastname, int age, String position, float speed, float passing, float defending, float shooting,
                  float goalkeeping, Boolean injured, Boolean suspended ) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.age = age;
        this.position = position;
        this.speed = speed;
        this.passing = passing;
        this.defending = defending;
        this.shooting = shooting;
        this.goalkeeping = goalkeeping;
        this.injured = injured;
        this.suspended = suspended;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public float getPassing() {
        return passing;
    }

    public void setPassing(float passing) {
        this.passing = passing;
    }

    public float getDefending() {
        return defending;
    }

    public void setDefending(float defending) {
        this.defending = defending;
    }

    public float getShooting() {
        return shooting;
    }

    public void setShooting(float shooting) {
        this.shooting = shooting;
    }

    public float getGoalkeeping() {
        return goalkeeping;
    }

    public void setGoalkeeping(float goalkeeping) {
        this.goalkeeping = goalkeeping;
    }

    public Boolean getInjured() {
        return injured;
    }

    public void setInjured(Boolean injured) {
        this.injured = injured;
    }

    public Boolean getSuspended() {
        return suspended;
    }

    public void setSuspended(Boolean suspended) {
        this.suspended = suspended;
    }

    public float getRating(){
        return rating;
    }

    public void setRating(float rating){
        this.rating = rating;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", age=" + age +
                ", position='" + position + '\'' +
                ", speed=" + speed +
                ", passing=" + passing +
                ", defending=" + defending +
                ", shooting=" + shooting +
                ", goalkeeping=" + goalkeeping +
                ", injured=" + injured +
                ", suspended=" + suspended +
                ", rating=" + rating +
                ", team=" + team +
                '}';
    }
}
